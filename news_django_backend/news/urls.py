from django.urls import path

from . import views

app_name = 'newsAPI'
urlpatterns = [
    path('users/me', views.UserMeAPIView.as_view(), name='user_me'),
]

from rest_framework import viewsets, status
from rest_framework.decorators import action
from rest_framework.parsers import MultiPartParser, FormParser
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework.response import Response

from .serializers import UserSerializer, NewsSerializer, NewsShortSerializer
from .models import CustomUser, News


class UserViewSet(viewsets.ModelViewSet):
    parser_classes = (MultiPartParser, FormParser)

    queryset = CustomUser.objects.all().order_by('-id')
    serializer_class = UserSerializer


class NewsViewSet(viewsets.ModelViewSet):
    parser_classes = (MultiPartParser, FormParser)

    queryset = News.objects.all().order_by('-date_time_created')
    serializer_class = NewsSerializer

    def get_permissions(self):
        if self.action == 'create':
            permission_classes = [IsAuthenticated]
        else:
            permission_classes = [AllowAny]
        return [permission() for permission in permission_classes]

    @action(detail=False)
    def author_specific(self, request):
        spec_news = News.objects\
            .filter(author=request.query_params['user'])\
            .order_by('-date_time_created')

        serializer = self.get_serializer(spec_news, many=True)
        return Response(serializer.data)

    def create(self, request, *args, **kwargs):
        serializer = NewsShortSerializer(data=request.data)
        if serializer.is_valid():
            author = CustomUser.objects.get(pk=request.user.pk)

            news = News.objects.create(
                theme=serializer.data['theme'],
                content=serializer.data['content'],
                tags=serializer.data['tags'],
                image=request.data.get('image', None),
                author=author
            )
            news.save()
            return Response(status=status.HTTP_201_CREATED)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

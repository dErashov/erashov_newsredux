from django.contrib import admin

# Register your models here.
from news.models import CustomUser, News

admin.site.register(CustomUser)
admin.site.register(News)

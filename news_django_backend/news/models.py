from django.db import models
from django.contrib.auth.models import AbstractUser, User

# Create your models here.


class CustomUser(AbstractUser):
    personalData = models.TextField(blank=True)
    avatar = models.ImageField(null=True, blank=True)
    avatarGoogle = models.CharField(max_length=1024, null=True, blank=True)


class News(models.Model):
    theme = models.CharField(max_length=256)
    content = models.TextField()
    tags = models.CharField(max_length=1024, null=True, blank=True)
    date_time_created = models.DateTimeField(auto_now_add=True)
    author = models.ForeignKey(CustomUser, on_delete=models.CASCADE)
    image = models.ImageField(null=True, blank=True)

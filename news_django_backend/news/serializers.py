from .models import CustomUser, News
from rest_framework import serializers, pagination


class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = CustomUser
        fields = ('url', 'username', 'first_name', 'last_name', 'email',
                  'personalData', 'avatar', 'avatarGoogle')


class NewsSerializer(serializers.HyperlinkedModelSerializer):
    authorFirstName = serializers.ReadOnlyField(source='author.first_name')
    authorLastName = serializers.ReadOnlyField(source='author.last_name')
    authorId = serializers.ReadOnlyField(source='author.id')

    class Meta:
        model = News
        fields = ('url', 'theme', 'content', 'tags', 'authorId',
                  'date_time_created', 'authorFirstName', 'authorLastName',
                  'image')


class NewsShortSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = News
        fields = ('theme', 'content', 'tags')

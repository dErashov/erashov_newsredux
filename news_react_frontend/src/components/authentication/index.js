import React from 'react';
import Modal from 'react-modal';
import LoginForm from "./loginForm";
import {connect} from "react-redux";
import {setAuthModalDisplayState} from "../../actions/auth";
import {AuthModalDisplayStates} from "../../actions/action-types";
import {modalStyles} from '../common/modalStyle'
import RegisterForm from "./RegisterForm";


class ConnectedAuthentication extends React.Component {

  handleCloseModal = () => {
    this.props.setAuthModalDisplayState(AuthModalDisplayStates.HIDE);
  };

  render() {
    const { displayModal: displayAuthMode } = this.props;
    const displayModal = displayAuthMode !== AuthModalDisplayStates.HIDE;

    return (
      <Modal
        isOpen={displayModal}
        onRequestClose={this.handleCloseModal}
        style={modalStyles}
      >
        {displayAuthMode === AuthModalDisplayStates.LOGIN ?
          <LoginForm/> :
          <RegisterForm/>
        }

      </Modal>
    )
  }
}

Modal.setAppElement('body');

const mapStateToProps = state => {
  return { displayModal: state.auth.modalDisplayMode}
};

const Authentication = connect(
  mapStateToProps,
  {setAuthModalDisplayState}
)(ConnectedAuthentication);

export default Authentication;
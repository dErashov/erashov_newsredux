import React from 'react'
import {GoogleLogin} from "react-google-login";
import {connect} from "react-redux";
import { tryAuthGoogle, tryLoginStd } from "../../actions/auth";

class ConnectedLoginForm extends React.Component {
  state = {
    login: '',
    password: '',
  };

  handleResponseGoogle = (res) => {
    console.log(res);
    this.props.tryAuthGoogle(res.accessToken);
  };

  handleInputsChange = (e) =>{
    const {name: inputName, value: inputValue} = e.target;
    this.setState(()=>{
      return {
        [inputName]: inputValue
      }
    })
  };

  handleFormSubmit = (e) => {
    e.preventDefault();
    const {login, password} = this.state;
    this.props.tryLoginStd(login, password);
  };

  render() {
    const {login, password} = this.state;
    return (
      <div className="">
        <h5 className="mb-4 text-center">Login</h5>
        <GoogleLogin
          className="container-fluid mx-auto mb-3"
          clientId="722554339505-121jq1boua7pl4lte5vr943f982uqov8.apps.googleusercontent.com"
          buttonText="Use Google Account"
          onSuccess={this.handleResponseGoogle}
          onFailure={this.handleResponseGoogle}
        />
        <form className="form-group"
              onSubmit={this.handleFormSubmit}>
          <input className="form-group form-control"
                 name="login"
                 onChange={this.handleInputsChange}
                 value={login}
                 type="text" placeholder="Login" aria-label="Search"
                 autoComplete="username"/>
          <input className="form-group form-control"
                 name="password"
                 onChange={this.handleInputsChange}
                 value={password}
                 type="password" placeholder="Password"
                 autoComplete="current-password"/>
          <button type="submit"
                 className="btn btn-primary">
            Login</button>
        </form>
      </div>
    )
  }
}

const LoginForm = connect(
  null,
  {tryLoginStd, tryAuthGoogle: tryAuthGoogle}
)(ConnectedLoginForm);

export default LoginForm;
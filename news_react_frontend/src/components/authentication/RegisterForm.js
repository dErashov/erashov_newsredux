import React from 'react'
import {GoogleLogin} from "react-google-login";
import {connect} from "react-redux";
import {tryAuthGoogle, tryRegisterStd} from "../../actions/auth";
import {RegisterResult} from "../../actions/action-types";

class ConnectedRegisterForm extends React.Component {
  state = {
    login: '',
    password: '',
    passwordRepeat: '',
    email: '',
  };

  handleResponseGoogle = (res) => {
    console.log(res);
    this.props.tryAuthGoogle(res.accessToken);
  };

  handleInputsChange = (e) =>{
    const {name: inputName, value: inputValue} = e.target;
    this.setState(()=>{
      return {
        [inputName]: inputValue
      }
    })
  };

  handleFormSubmit = (e) => {
    e.preventDefault();
    const {login, password, passwordRepeat, email} = this.state;
    this.props.tryRegisterStd(login, password, passwordRepeat, email);
  };

  render() {
    const {login, password, passwordRepeat, email} = this.state;
    const {registerStatus, registerMessage} = this.props;

    console.log(registerMessage);
    let registerMessages;
    if(registerMessage) {
      registerMessages = Object.keys(registerMessage).map(function (key) {
        return [key, registerMessage[key]];
      });
    }

    return (
      <div className="">
        <h5 className="mb-4 text-center">Register</h5>
        <GoogleLogin
          className="container-fluid mx-auto mb-3"
          clientId="722554339505-121jq1boua7pl4lte5vr943f982uqov8.apps.googleusercontent.com"
          buttonText="Use Google Account"
          onSuccess={this.handleResponseGoogle}
          onFailure={this.handleResponseGoogle}
        />
        <form className="form-group"
              onSubmit={this.handleFormSubmit}>
          <input className="form-group form-control"
                 name="login"
                 onChange={this.handleInputsChange}
                 value={login}
                 type="text" placeholder="Login"
                 autoComplete="username"/>
          <input className="form-group form-control"
                 name="password"
                 onChange={this.handleInputsChange}
                 value={password}
                 type="password" placeholder="Password"
                 autoComplete="password"/>
          <input className="form-group form-control"
                 name="passwordRepeat"
                 onChange={this.handleInputsChange}
                 value={passwordRepeat}
                 type="password" placeholder="Repeat Password"
                 autoComplete="password"/>
          <input className="form-group form-control"
                 name="email"
                 onChange={this.handleInputsChange}
                 value={email}
                 type="email" placeholder="Email"
                 autoComplete="email"/>
          {registerStatus !== RegisterResult.SUCCESS ?
            <button type="submit"
                    className="btn btn-primary">
              Register</button> :
            <p>Successful<br/>Now you can log in</p>
          }
          {registerStatus === RegisterResult.ERROR &&
            registerMessages.map((item,index) => (
              <p key={index}>{item}</p>
            ))
          }
        </form>
      </div>
    )
  }
}

const mapStateToProps = state => ({
  registerStatus: state.auth.registerStatus.status,
  registerMessage: state.auth.registerStatus.message,
});

const RegisterForm = connect(
  mapStateToProps,
  {tryRegisterStd, tryAuthGoogle}
)(ConnectedRegisterForm);

export default RegisterForm;
import {Route, Link} from "react-router-dom";
import newsSearch from "../feed/newsSearch";
import Authentication from "../authentication";
import React from "react";
import {AuthModalDisplayStates, LoginStatus} from "../../actions/action-types";
import {connect} from "react-redux";
import {logout, setAuthModalDisplayState} from "../../actions/auth";

class ConnectedNavbar extends React.Component{
  handleLoginClick = () => {
    this.props.setAuthModalDisplayState(AuthModalDisplayStates.LOGIN);
  };

  handleRegisterClick = () => {
    this.props.setAuthModalDisplayState(AuthModalDisplayStates.REGISTER);
  };

  handleLogoutClick = () => {
    this.props.logout();
  };

  render() {
    const {user, loginStatus} = this.props;
    const {username:login, pk} = user;
    const isLoggedIn = loginStatus === LoginStatus.LOGGED_IN;
    const userPageLink = `/users/${pk}/`;

    return (<nav className="navbar navbar-expand-md navbar-dark bg-dark">
      <a className="navbar-brand" href="/">Life Journal</a>
      <button className="navbar-toggler" type="button" data-toggle="collapse">
        <span className="navbar-toggler-icon"/>
      </button>
      <div className="collapse navbar-collapse" id="navbarSupportedContent">
        <ul className="navbar-nav mr-auto">
          <li className="nav-item">
            <Link className="nav-link" to="/">Feed</Link>
          </li>
        </ul>
        <Route exact path="/" component={newsSearch}/>

        <div className="dropdown ml-4">
          <button className="btn btn-primary dropdown-toggle" type="button" id="authDropdown"
                  data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            {isLoggedIn ?
              login :
              'Guest'
            }</button>
          {!isLoggedIn ?
            <div className="dropdown-menu dropdown-menu-right" aria-labelledby="authDropdown">
              <button className="dropdown-item"
                      type="button"
                      onClick={this.handleLoginClick}>
                Login
              </button>
              <button className="dropdown-item"
                      type="button"
                      onClick={this.handleRegisterClick}>
                Register
              </button>
            </div>
            :
            <div className="dropdown-menu dropdown-menu-right" aria-labelledby="authDropdown">
              <Link to={userPageLink} className="dropdown-item"
                      >My Page</Link>
              <button className="dropdown-item"
                      type="button"
                      onClick={this.handleLogoutClick}>
                Log out
              </button>
            </div>
          }
        </div>

        <Authentication>
        </Authentication>
      </div>
    </nav>)
  }
}

const mapStateToProps = state => ({
  loginStatus: state.auth.loginStatus,
  user: state.auth.user,
});

const Navbar = connect(
  mapStateToProps,
  { setAuthModalDisplayState, logout}
)(ConnectedNavbar);

export default Navbar;
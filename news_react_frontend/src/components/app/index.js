import React from 'react';
import { Route } from 'react-router-dom'
import './App.css';

import userPage from '../userPage/index';
import Feed from '../feed/index';
import Navbar from "./navbar";
import {connect} from "react-redux";
import {fetchCurrentUser, tryResumeSession} from "../../actions/auth";


class ConnectedApp extends React.Component {

  componentDidMount() {
    this.props.tryResumeSession();
    this.props.fetchCurrentUser();
  }

  render() {

    return (
    <div>
      <header>
        <Navbar/>
      </header>

      <main style={{
        'maxWidth': '800px',
        'margin': '0 auto'}}>
        <Route exact path="/" component={Feed}/>
        <Route path="/users/:id/" component={userPage}/>
      </main>
    </div>
    )
  }
}

const App = connect(
  null,
  {tryResumeSession, fetchCurrentUser}
)(ConnectedApp);

export default App;

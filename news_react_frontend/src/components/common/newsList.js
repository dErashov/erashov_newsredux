import {Link} from "react-router-dom";
import ReactPaginate from "react-paginate";
import React from "react";

export function NewsList(props) {
  const { news, newsCount } = props;

  return (
    <div>
      <ul className="list-group list-group-flush">
        {news.map(item => (
          <li className="list-group-item" key={item.url}>
            <div className="container-fluid">
              <div className="row">
                <Link to={`/users/${item.authorId}/`} className="mr-4">
                  {`${item.authorFirstName} ${item.authorLastName}`}</Link>
                <p className="mr-2">{
                  (new Date(Date.parse(item.date_time_created)))
                    .toLocaleString('ru')
                }</p>
              </div>
            </div>
            <h2>{item.theme}</h2>
            <p>{item.content}</p>
            { item.image &&
            <div>
              <img src={item.image} style={{width: "400px"}} alt="News"/>
              <br/>
            </div>
            }
            {item.tags && item.tags.split(' ').map((item, index)=> (
              <span
                className="badge badge-dark mr-2"
                key={index}>
                    {item}
                  </span>
            ))}
          </li>
        ))}
      </ul>
      <ReactPaginate
        pageCount = {Math.ceil(newsCount / 5)}
        marginPagesDisplayed={2}
        pageRangeDisplayed={5}
        onPageChange={props.handlePageClick}
        containerClassName={'pagination justify-content-center'}
        pageClassName={'page-item'}
        pageLinkClassName={'page-link'}
        previousClassName={'page-link'}
        nextClassName={'page-link'}
      />
    </div>
  )
}
import React from 'react'
import {connect} from "react-redux";
import {fetchUserNews} from "../../actions/profile";
import {NewsList} from "../common/newsList";

class ConnectedUserNews extends React.Component {
  state = {
    currentPage: 1
  };

  componentDidMount() {
    this.props.fetchUserNews(this.props.userId, this.state.currentPage);
  }

  componentDidUpdate(prevProps) {
    if (prevProps.userId !== this.props.userId) {
      this.props.fetchUserNews(this.props.userId, this.state.currentPage);
    }
  }

  handlePageClick = (data) => {
    this.props.fetchUserNews(this.props.userId, (data.selected+1));
  };

  render() {
    const { userNews } = this.props;
    const newsCount = userNews.length;

    return <NewsList news={userNews}
                     newsCount={newsCount}
                     handlePageClick={this.handlePageClick}/>
  }
}

const mapStateToProps = state => ({
  userNews: state.profile.news,
});

const UserNews = connect(
  mapStateToProps,
  { fetchUserNews }
)(ConnectedUserNews);

export default UserNews;
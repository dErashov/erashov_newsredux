import React from "react";
import {connect} from "react-redux";
import {editProfile} from "../../actions/profile";

class ConnectedEditProfile extends React.Component {
  constructor(props) {
    super(props);

    const {first_name, last_name, personalData} = this.props.user;

    this.state = {
      firstName: first_name,
      lastName: last_name,
      personalData: personalData,
      avatar: undefined,
    }
  }

  handleInputsChange = (e) =>{
    const {name: inputName, value: inputValue} = e.target;
    this.setState(()=>{
      return {
        [inputName]: inputValue
      }
    })
  };

  handleFileChange = e => {
    const avatar = e.target.files[0];
    this.setState(()=>{
      return {
        avatar
      }
    })
  };

  handleFormSubmit = (e) => {
    e.preventDefault();
    const {firstName, lastName, personalData, avatar} = this.state;
    const {loggedUserId, editProfile} = this.props;

    const newUser = {
      first_name: firstName,
      last_name: lastName,
      personalData,
      avatar,
    };

    editProfile(newUser, loggedUserId);
  };

  render() {
    const {firstName, lastName, personalData} = this.state;
    return (
      <div style={{width: "600px"}}>
        <h5 className="mb-4 text-center">Edit profile</h5>

        <form className="form-group"
              onSubmit={this.handleFormSubmit}>
          <input className="form-group form-control"
                 name="firstName"
                 onChange={this.handleInputsChange}
                 value={firstName}
                 type="text" placeholder="First name"/>
          <input className="form-group form-control"
                 name="lastName"
                 onChange={this.handleInputsChange}
                 value={lastName}
                 type="text" placeholder="Last name"/>
          <textarea className="form-group form-control"
                 rows={10}
                 name="personalData"
                 onChange={this.handleInputsChange}
                 value={personalData}
                 placeholder="About you"/>
          <div className="form-group">
            <label htmlFor="newsImage">Change avatar</label>
            <input className="form-control-file" name="image"
                   id="newsImage" type="file"
                   onChange={this.handleFileChange}
            />
          </div>
          <button type="submit"
                  className="btn btn-primary">
            Save</button>
        </form>
      </div>
    )
  }
}

const mapStateToProps = state => ({
  user: state.profile.user,
  loggedUserId: state.auth.user.pk,
});

const EditProfile = connect(
  mapStateToProps,
  {editProfile}
)(ConnectedEditProfile);

export default EditProfile;
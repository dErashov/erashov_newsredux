import React from 'react'
import { connect } from 'react-redux'
import Profile from "./profile";
import UserNews from "./userNews";
import {modalStyles} from "../common/modalStyle";
import {ProfileModalsDisplayMode} from "../../actions/action-types";
import Modal from "react-modal";
import {setProfileModalsDisplayState} from "../../actions/profile";
import AddNews from "./addNews";
import EditProfile from "./editProfile";

class Home extends React.Component {

  handleAddNewsClick = () => {
    this.props.setProfileModalsDisplayState(ProfileModalsDisplayMode.ADD_NEWS);
  };

  handleEditProfileClick = () => {
    this.props.setProfileModalsDisplayState(ProfileModalsDisplayMode.EDIT_PROFILE);
  };

  handleCloseModal = () => {
    this.props.setProfileModalsDisplayState(ProfileModalsDisplayMode.HIDE);
  };

  render() {
    const { modalsState, loggedUserId, match } = this.props;
    const { id: paramUserId} = match.params;
    const displayModal = modalsState !== ProfileModalsDisplayMode.HIDE;
    const isLoggedUserProfile = Number(paramUserId) === loggedUserId;
    return (
      <div>
        <Profile userId={paramUserId}/>
          {isLoggedUserProfile &&
          <div className="row align-content-center">
              <button className="col btn btn-primary m-3"
                onClick={this.handleAddNewsClick}>
                Add news</button>

              <button className="col btn btn-primary m-3"
                onClick={this.handleEditProfileClick}>
                Edit profile</button>
            </div>
          }
        <UserNews userId={this.props.match.params.id}/>

        <Modal
          isOpen={displayModal}
          onRequestClose={this.handleCloseModal}
          style={modalStyles}
        >
          {modalsState === ProfileModalsDisplayMode.ADD_NEWS ?
            <AddNews/> :
            <EditProfile/>
          }
        </Modal>
      </div>
    )
  }
}

const mapStateToProps = state => ({
  modalsState: state.profile.modalsState,
  loggedUserId: state.auth.user.pk,
});

export default connect(
  mapStateToProps,
  { setProfileModalsDisplayState }
)(Home)
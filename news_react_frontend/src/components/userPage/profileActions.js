import React from 'react'
import Modal from "react-modal";
import {modalStyles} from "../common/modalStyle";
import {ProfileModalsDisplayMode} from "../../actions/action-types";
import {connect} from "react-redux";
import AddNews from "./addNews";
import EditProfile from "./editProfile";

class ConnectedProfileActions extends React.Component {
  render() {
    const {modalsState} = this.props;
    const displayModal = modalsState !== ProfileModalsDisplayMode.HIDE;


    return (
      <div>
        <div className="row align-content-center">
          <button className="col btn btn-primary m-3">Add news</button>
          <button className="col btn btn-primary m-3">Edit profile</button>
        </div>

        <Modal
          isOpen={displayModal}
          onRequestClose={this.handleCloseModal}
          style={modalStyles}
        >
          {modalsState === ProfileModalsDisplayMode.ADD_NEWS ?
            <AddNews/> :
            <EditProfile/>
          }
        </Modal>
      </div>
    )
  }
}

const mapStateToProps = state => ({
  modalsState: state.profile.modalsState,
});

const ProfileActions = connect(
  mapStateToProps,
  null
)(ConnectedProfileActions);

export default ProfileActions;
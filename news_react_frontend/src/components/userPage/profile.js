import React from 'react'
import {connect} from "react-redux";
import {fetchUserProfile} from "../../actions/profile";

class ConnectedProfile extends React.Component {
  componentDidMount() {
    this.props.fetchUserProfile(this.props.userId)
  }

  componentDidUpdate(prevProps) {
    if (prevProps.userId !== this.props.userId) {
      this.props.fetchUserProfile(this.props.userId)
    }
  }

  render() {
    const {first_name, last_name, personalData, avatar} = this.props.userProfileData;

    return (<div className="container">
      <div className="row pt-5">
        <div className="col-sm">
          <img src={avatar} style={{width: "300px"}} alt="No avatar"/>
        </div>
        <div className="col-sm">
          <h3>{first_name + ' ' + last_name}</h3>
          <p>{personalData}</p>
        </div>
      </div>
    </div>)
  }
}

const mapStateToProps = state => ({
  userProfileData: state.profile.user,
});

const Profile = connect(
  mapStateToProps,
  { fetchUserProfile }
)(ConnectedProfile);

export default Profile;
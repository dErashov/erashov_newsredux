import React from "react";
import {connect} from "react-redux";
import {postNews, setProfileModalsDisplayState} from "../../actions/profile";

class ConnectedAddNews extends React.Component {
  state = {
    theme: '',
    content: '',
    tags: '',
    image: null,
  };


  handleInputsChange = (e) =>{
    const {name: inputName, value: inputValue} = e.target;
    this.setState(()=>{
      return {
        [inputName]: inputValue
      }
    })
  };

  handleFileChange = e => {
    const image = e.target.files[0];
    this.setState(()=>{
      return {
        image
      }
    })
  };

  handleFormSubmit = (e) => {
    e.preventDefault();
    const {theme, content, tags, image} = this.state;
    this.props.postNews({theme, content, tags, image });
  };

  render() {
    const {theme, content, tags} = this.state;
    return (
      <div style={{width: "600px"}}>
        <h5 className="mb-4 text-center">What's new?</h5>

        <form className="form-group"
              onSubmit={this.handleFormSubmit}>
          <input className="form-group form-control"
                 name="theme"
                 onChange={this.handleInputsChange}
                 value={theme}
                 type="text" placeholder="Theme"/>
          <textarea className="form-group form-control"
                  rows="10"
                 name="content"
                 onChange={this.handleInputsChange}
                 value={content}
                 placeholder="Content"/>
          <input className="form-group form-control"
                 name="tags"
                 onChange={this.handleInputsChange}
                 value={tags}
                 type="text" placeholder="Tags"/>
          <div className="form-group">
            <label htmlFor="newsImage">Add image to post</label>
            <input className="form-control-file" name="image"
                   id="newsImage" type="file"
                   onChange={this.handleFileChange}
            />
          </div>
          <button type="submit"
                  className="btn btn-primary">
            Post it!</button>
        </form>
      </div>
    )
  }
}

const AddNews = connect(
  null,
  { postNews, setProfileModalsDisplayState }
)(ConnectedAddNews);

export default AddNews;
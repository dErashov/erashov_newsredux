import React from 'react'
import {setSearchFilter, setSearchWord} from "../../actions";
import {connect} from "react-redux";
import {NewsSearchFilters} from "../../actions/action-types";

function mapDispatchToProps(dispatch) {
  return {
    changeSearchFilter: filter => dispatch(setSearchFilter(filter)),
    changeSearchWord: payload => dispatch(setSearchWord(payload))
  }
}

class newsSearchConnected extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      filterMode: 'All',
      searchWord: '',
    };

    this.handleSearchModeChange = this.handleSearchModeChange.bind(this);
    this.handleSearchWordChange = this.handleSearchWordChange.bind(this);
  }

  handleSearchModeChange(e){
    const mode = e.target.name;
    this.setState({filterMode: mode});
    let actionNewsSearchFilter;
    switch (mode) {
      case 'All':
        actionNewsSearchFilter = NewsSearchFilters.ALL;
        break;
      case 'Tags':
        actionNewsSearchFilter = NewsSearchFilters.TAGS;
        break;
      case 'Author':
        actionNewsSearchFilter = NewsSearchFilters.AUTHOR;
        break;
      default:
        return;
    }
    this.props.changeSearchFilter(actionNewsSearchFilter);
  }

  handleSearchWordChange(e){
    const searchWord = e.target.value;
    this.setState({searchWord: searchWord});
    this.props.changeSearchWord(searchWord);
  }

  render() {
    const {filterMode, searchWord} = this.state;
    return (
      <div className='row align-items-center'>
        <div className="dropdown mx-2">
          <button className="btn btn-secondary dropdown-toggle" id="searchModeDropdown" data-toggle="dropdown"
             aria-haspopup="true" aria-expanded="false">
            {filterMode}
          </button>
          <div className="dropdown-menu" aria-labelledby="searchModeDropdown">
            <button className="dropdown-item" name="All" onClick={this.handleSearchModeChange}>All</button>
            <button className="dropdown-item" name="Tags" onClick={this.handleSearchModeChange}>Tags</button>
            <button className="dropdown-item" name="Author" onClick={this.handleSearchModeChange}>Authors</button>
          </div>
        </div>
        <form className="form-inline my-2 my-lg-0">
          <input className="form-control mr-sm-2"
                 onChange={this.handleSearchWordChange}
                 value={searchWord}
                 type="search" placeholder="Search"  aria-label="Search"/>
        </form>
      </div>
    );
  }
}

const newsSearch = connect(null, mapDispatchToProps)(newsSearchConnected);

export default newsSearch;
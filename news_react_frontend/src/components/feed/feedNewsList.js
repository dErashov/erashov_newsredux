import React from 'react'
import {connect} from 'react-redux'
import {getNewsData} from "../../actions";
import {NewsSearchFilters} from "../../actions/action-types";
import {NewsList} from "../common/newsList";

class ConnectedFeedNewsList extends React.Component{
  constructor(props){
    super(props);
    this.filterNewsList = this.filterNewsList.bind(this);
    this.handlePageClick = this.handlePageClick.bind(this);
  }

  componentDidMount() {
    this.props.getNewsData();
  }

  filterNewsList(news){
    const {filter, searchWord: searchWordData} = this.props;
    let filteredNews = news;

    if(searchWordData) {
      const searchWord = searchWordData.toLowerCase();
      switch (filter) {
        case NewsSearchFilters.ALL:
          filteredNews = news.filter(newsItem => {
            return newsItem.authorFirstName.toLowerCase().includes(searchWord) ||
              newsItem.theme.toLowerCase().includes(searchWord) ||
              newsItem.content.toLowerCase().includes(searchWord) ||
              newsItem.tags.toLowerCase().includes(searchWord)
          });
          break;
        case NewsSearchFilters.AUTHOR:
          filteredNews = news.filter(newsItem => {
            return newsItem.authorFirstName.toLowerCase().includes(searchWord) ||
              newsItem.authorLastName.toLowerCase().includes(searchWord)
          });
          break;
        case NewsSearchFilters.TAGS:
          filteredNews = news.filter(newsItem => {
            return newsItem.tags.toLowerCase().includes(searchWord);
          });
          break;
        default:
          break;
      }
    }
    return filteredNews;
  }

  handlePageClick(data){
    this.props.getNewsData(data.selected+1);
  }

  render() {
    const { news: newsRaw, newsCount} = this.props;
    const news = this.filterNewsList(newsRaw);

    return <NewsList news={news}
                     newsCount={newsCount}
                     handlePageClick={this.handlePageClick}/>
  }
}

const mapStateToProps = state => {
  return { news: state.news.news,
      newsCount: state.news.count,
      filter: state.searchFilter,
      searchWord: state.searchWord}
};

const FeedNewsList = connect(
  mapStateToProps,
  { getNewsData }
)(ConnectedFeedNewsList);

export default FeedNewsList;
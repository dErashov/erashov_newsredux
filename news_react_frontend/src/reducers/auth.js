import {
  AuthModalDisplayStates, FETCH_CURRENT_USER_RESPONDED,
  LOGIN_RESPONDED,
  LoginStatus, LOGOUT_RESPONDED, REGISTER_RESPONDED,
  SET_AUTH_MODAL_DISPLAY_STATE, RESTORE_TOKEN, FetchCurrentUserResponseStatus
} from "../actions/action-types";

const initialState = {
  modalDisplayMode: AuthModalDisplayStates.HIDE,
  loginStatus: LoginStatus.GUEST,
  registerStatus: {status: undefined, message: undefined},
  token: null,
  user: { login: '',
          },
};

export function auth(state = initialState, action) {
  switch(action.type){

    case SET_AUTH_MODAL_DISPLAY_STATE:
      return Object.assign({}, state,{
        modalDisplayMode: action.mode
      });


    case LOGIN_RESPONDED: {
      const newState = {
        loginStatus: action.status,
      };
      if (action.status === LoginStatus.LOGGED_IN) {
        newState.user = action.user;
        newState.token = action.token;

        localStorage.setItem('access_token', newState.token);

        newState.modalDisplayMode = AuthModalDisplayStates.HIDE;
      }
      return Object.assign({}, state, newState);
    }


    case REGISTER_RESPONDED:
      return Object.assign({}, state, {
        registerStatus: {
          status: action.status,
          message: action.details,
        }
      });

    case LOGOUT_RESPONDED: {
      if(action.success) {
        const newState = {
          loginStatus: LoginStatus.GUEST,
          user: { login: '',
                  token: null,}
        };
        localStorage.removeItem('access_token');
        return Object.assign({}, state, newState);
      }
      return state;
    }

    case RESTORE_TOKEN: {
      const token = localStorage.getItem('access_token');
      if(token) {
        const newState = {
          token,
        };
        return Object.assign({}, state, newState);
      }
      return state;
    }


    case FETCH_CURRENT_USER_RESPONDED: {
      if(action.status === FetchCurrentUserResponseStatus.LOGGED_IN){

        const fetchedUser = action.user;
        fetchedUser.token = state.user.token;

        const newState = {
          loginStatus: LoginStatus.LOGGED_IN,
          user: fetchedUser,
        };
        return Object.assign({}, state, newState);
      }
      return state;
    }

    default:
      return state;
  }
}

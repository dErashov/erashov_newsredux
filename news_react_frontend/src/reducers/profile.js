import {
  FETCH_USER_NEWS_RESPONDED,
  FETCH_USER_PROFILE_RESPONDED,
  ProfileModalsDisplayMode,
  SET_PROFILE_MODALS_DISPLAY_STATE
} from "../actions/action-types";

const initialState = {
  modalsState: ProfileModalsDisplayMode.HIDE,
  user: {},
  news: [],
};

export function profile(state = initialState, action) {
  switch (action.type) {

    case FETCH_USER_PROFILE_RESPONDED: {
      const newState = {
        user: action.user,
      };

      newState.user.avatar =
        action.user.avatar ? action.user.avatar :
        action.user.avatarGoogle ? action.user.avatarGoogle :
        'http://s3.amazonaws.com/37assets/svn/765-default-avatar.png';
      return Object.assign({}, state, newState);
    }

    case FETCH_USER_NEWS_RESPONDED: {
      const newState = {
        news: action.news,
      };
      return Object.assign({}, state, newState);
    }

    case SET_PROFILE_MODALS_DISPLAY_STATE: {
      const newState = {
        modalsState: action.state,
      };
      return Object.assign({}, state, newState);
    }

    default: {
      return state;
    }
  }
}
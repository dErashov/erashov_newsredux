import {NEWS_FETCH_SUCCEEDED} from "../actions/action-types";

const newsInitialState = {
  count: 0,
  news: []
};

export function news(state = newsInitialState, action) {
  switch (action.type) {
    /*case ADD_NEWS:
      return state.concat(action.payload);*/

    case NEWS_FETCH_SUCCEEDED:
      return {
        count: action.payload.data.count,
        news: action.payload.data.results,
      };

    default:
      return state;
  }
}
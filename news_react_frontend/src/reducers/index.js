import { combineReducers } from 'redux'

import {
  NewsSearchFilters,
  SET_NEWS_SEARCH_FILTER,
  SET_SEARCH_KEYWORD
} from '../actions/action-types'

import {news} from "./news";
import {auth} from "./auth";
import {profile} from "./profile";

function searchFilter(state = NewsSearchFilters.ALL, action) {
  switch (action.type) {
    case SET_NEWS_SEARCH_FILTER:
      return action.filter;
    default:
      return state;
  }
}

function searchWord(state = null, action) {
  if(action.type === SET_SEARCH_KEYWORD){
    return action.payload;
  } else {
    return state;
  }
}

export default (history) => combineReducers({
  searchFilter,
  searchWord,
  news,
  auth,
  profile,
})

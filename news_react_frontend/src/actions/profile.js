import {
  EDIT_PROFILE,
  FETCH_USER_NEWS,
  FETCH_USER_PROFILE,
  POST_NEWS,
  SET_PROFILE_MODALS_DISPLAY_STATE
} from "./action-types";

export function fetchUserProfile(id){
  return {type: FETCH_USER_PROFILE,
          id,}
}

export function fetchUserNews(id, page) {
  return {type: FETCH_USER_NEWS,
  id,
  page}
}

export function setProfileModalsDisplayState(state) {
  return {type: SET_PROFILE_MODALS_DISPLAY_STATE,
  state}
}

export function postNews( news ) {
  return { type: POST_NEWS,
  news }
}

export function editProfile(user, id) {
  console.log(user);
  console.log(id);
  return { type: EDIT_PROFILE,
    user,
    userId: id,
  }
}
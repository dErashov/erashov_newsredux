/*
* Action types
*/

//======================= Feed =======================

export const ADD_NEWS = "ADD_NEWS";
export const NEWS_FETCH_SUCCEEDED = "NEWS_FETCH_SUCCEEDED";
export const NEWS_FETCH_FAILED = "NEWS_FETCH_FAILED";
export const NEWS_REQUESTED = "NEWS_REQUESTED";

export const SET_NEWS_SEARCH_FILTER = "SET_NEWS_SEARCH_FILTER";
export const SET_SEARCH_KEYWORD = "SET_SEARCH_KEYWORD";

export const NewsSearchFilters = {
  ALL: 'ALL',
  TAGS: 'TAGS',
  AUTHOR: 'AUTHOR',
};

//======================= Auth =======================

export const SET_AUTH_MODAL_DISPLAY_STATE = 'SET_AUTH_MODAL_DISPLAY_STATE';
export const AuthModalDisplayStates = {
  HIDE: 'HIDE',
  LOGIN: 'LOGIN',
  REGISTER: 'REGISTER',
};

export const TRY_LOGIN_STANDARD = 'TRY_LOGIN_STANDARD';
export const TRY_AUTH_GOOGLE = 'TRY_AUTH_GOOGLE';
export const TRY_REGISTER_STANDARD = 'TRY_REGISTER_STANDARD';

export const LOGIN_RESPONDED = 'LOGIN_RESPONDED';
export const LoginStatus = {
  GUEST: 'GUEST',
  LOGGED_IN: 'LOGGED_IN',
  WRONG_LOGIN: 'WRONG_LOGIN',
};

export const REGISTER_RESPONDED = 'REGISTER_RESPONDED';
export const RegisterResult = {
  NONE: 'NONE',
  SUCCESS: 'SUCCESS',
  ERROR: 'ERROR',
};

export const RESTORE_TOKEN = 'RESTORE_TOKEN';

export const FETCH_CURRENT_USER = 'FETCH_CURRENT_USER';
export const FETCH_CURRENT_USER_RESPONDED = 'FETCH_CURRENT_USER_RESPONDED';
export const FetchCurrentUserResponseStatus = {
  LOGGED_IN: 'LOGGED_IN',
  INVALID_TOKEN: 'INVALID_TOKEN',
  NO_TOKEN: 'NO_TOKEN',
};

export const LOGOUT = 'LOGOUT';
export const LOGOUT_RESPONDED = 'LOGOUT_RESPONDED';

//======================= Profile =======================

export const FETCH_USER_PROFILE = 'FETCH_USER_PROFILE';
export const FETCH_USER_PROFILE_RESPONDED = 'FETCH_USER_PROFILE_RESPONDED';

export const FETCH_USER_NEWS = 'FETCH_USER_NEWS';
export const FETCH_USER_NEWS_RESPONDED = 'FETCH_USER_NEWS_RESPONDED';

export const SET_PROFILE_MODALS_DISPLAY_STATE = 'SET_PROFILE_MODALS_DISPLAY_STATE';
export const ProfileModalsDisplayMode = {
  HIDE: 'HIDE',
  ADD_NEWS: 'ADD_NEWS',
  EDIT_PROFILE: 'EDIT_PROFILE',
};

export const EDIT_PROFILE = 'EDIT_PROFILE';

export const POST_NEWS = "POST_NEWS";
import {ADD_NEWS, SET_NEWS_SEARCH_FILTER, NEWS_REQUESTED, SET_SEARCH_KEYWORD} from "./action-types";


/*
* Action generators
*/
export function addNews(theme, content, tags) {
  return {
    type: ADD_NEWS,
    payload: {theme,
              content,
              tags}
  }
}

export function getNewsData(page) {
  return {type: NEWS_REQUESTED,
          page}
}

export function setSearchFilter(filter) {
  return {
    type: SET_NEWS_SEARCH_FILTER,
    filter
  }
}

export function setSearchWord(payload) {
  return {
    type: SET_SEARCH_KEYWORD,
    payload
  }
}
import {
  LOGOUT,
  SET_AUTH_MODAL_DISPLAY_STATE,
  TRY_AUTH_GOOGLE,
  TRY_LOGIN_STANDARD,
  TRY_REGISTER_STANDARD,
  RESTORE_TOKEN, FETCH_CURRENT_USER
} from "./action-types";

export function setAuthModalDisplayState(mode) {
  return {type: SET_AUTH_MODAL_DISPLAY_STATE,
    mode}
}

export function tryLoginStd(login, password) {
  return {type: TRY_LOGIN_STANDARD,
    login,
    password,
  }
}

export function tryRegisterStd(login, password1, password2, email) {
  return {type: TRY_REGISTER_STANDARD,
    login,
    password1,
    password2,
    email,
  }
}

export function tryAuthGoogle(accessToken) {
  return {
    type: TRY_AUTH_GOOGLE,
    accessToken,
  }
}

export function tryResumeSession() {
 return {type: RESTORE_TOKEN}
}

export function logout() {
  return {type: LOGOUT}
}

export function fetchCurrentUser() {
  return {type: FETCH_CURRENT_USER}
}
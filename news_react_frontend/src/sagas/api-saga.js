import { put, takeEvery, call, select } from 'redux-saga/effects'
import axios from 'axios/index';
import {
  EDIT_PROFILE,
  FETCH_CURRENT_USER,
  FETCH_CURRENT_USER_RESPONDED, FETCH_USER_NEWS, FETCH_USER_NEWS_RESPONDED,
  FETCH_USER_PROFILE,
  FETCH_USER_PROFILE_RESPONDED,
  FetchCurrentUserResponseStatus,
  LOGIN_RESPONDED,
  LoginStatus,
  LOGOUT,
  LOGOUT_RESPONDED,
  NEWS_FETCH_FAILED,
  NEWS_FETCH_SUCCEEDED,
  NEWS_REQUESTED, POST_NEWS, ProfileModalsDisplayMode,
  REGISTER_RESPONDED,
  RegisterResult, SET_PROFILE_MODALS_DISPLAY_STATE,
  TRY_AUTH_GOOGLE,
  TRY_LOGIN_STANDARD,
  TRY_REGISTER_STANDARD
} from "../actions/action-types";

const addrBase = 'http://192.168.2.135:8000/';
const takeTokenFromState = state => state.auth.token;
const takeUserIdFromState = state => state.auth.user.pk;

const clientID = '722554339505-121jq1boua7pl4lte5vr943f982uqov8.apps.googleusercontent.com';
const clientSecret = 'CUmlV2zeiPUaFzH9L77v95Xx';

function* fetchNews({page = 1}) {
  try {
    const url = `${addrBase}news/?page=${page}`;
    const payload = yield call(axios.get, url);
    yield put({type: NEWS_FETCH_SUCCEEDED, payload});
  } catch (e) {
    yield put({type: NEWS_FETCH_FAILED, message: e.message});
  }
}

function* tryLoginStd({login, password}) {
  const options1 = {
    method: 'POST',
    url: `${addrBase}auth/token`,
    data: {
      client_id: clientID,
      client_secret: clientSecret,
      grant_type: 'password',
      username: login,
      password,
    }
  };
  const options2 = {
    method: 'GET',
    url: `${addrBase}rest-auth/user/`,
  };

  try {
    const tokenResp = yield call(axios, options1);

    options2.headers = {Authorization: `Bearer ${tokenResp.data.access_token}`};
    const userResp = yield call(axios, options2);

    yield put({type: LOGIN_RESPONDED,
      status: LoginStatus.LOGGED_IN,
      token: tokenResp.data.access_token,
      user: userResp.data,
    });
  }
  catch (e) {
    const sagaResp = {
      status: LoginStatus.WRONG_LOGIN
    };
    yield put({type: LOGIN_RESPONDED, sagaResp})
  }
}

function* tryLoginGoogle({accessToken}) {
  const options1 = {
    method: 'POST',
    url: `${addrBase}auth/convert-token`,
    data: {
      client_id: clientID,
      client_secret: clientSecret,
      grant_type: 'convert_token',
      backend: 'google-oauth2',
      token: accessToken,
    }
  };
  const options2 = {
    method: 'GET',
    url: `${addrBase}rest-auth/user/`,
  };
  try {
    const tokenResp = yield call(axios, options1);

    options2.headers = {Authorization: `Bearer ${tokenResp.data.access_token}`};
    const userResp = yield call(axios, options2);

    yield put({type: LOGIN_RESPONDED,
      status: LoginStatus.LOGGED_IN,
      token: tokenResp.data.access_token,
      user: userResp.data,
      });
  }
  catch (e) {
    const sagaResp = {
      status: LoginStatus.WRONG_LOGIN
    };
    yield put({type: LOGIN_RESPONDED, sagaResp})
  }
}

function* tryRegisterStd({login, password1, password2, email}) {
  const options = {
    method: 'POST',
    url: `${addrBase}rest-auth/registration/`,
    data: {
      username: login,
      password1,
      password2,
      email,
    }
  };
  try {
    yield call(axios, options);
    yield put({
      type: REGISTER_RESPONDED,
      status: RegisterResult.SUCCESS
    })
  } catch (e) {
    yield put({
      type: REGISTER_RESPONDED,
      status: RegisterResult.ERROR,
      details: e.response.data,
    })
  }
}

function* logout(){
  const token = yield select(takeTokenFromState);

  const options = {
    method: 'POST',
    url: `${addrBase}auth/revoke-token`,
    data: {
      client_id: clientID,
      client_secret: clientSecret,
      token: token,
    }
  };
  try {
    yield call(axios, options);
    yield put({type: LOGOUT_RESPONDED, success: true});
  } catch (e) {
    yield put({type: LOGOUT_RESPONDED, success: false});
    console.log(e);
  }
}

function* getCurrentUserData() {
  const token = localStorage.getItem('access_token');

  if(token) {
    const options = {
      method: 'GET',
      url: `${addrBase}rest-auth/user/`,
      headers: {Authorization: `Bearer ${token}`},
    };

    try {
      const resp = yield call(axios, options);

      yield put({
        type: FETCH_CURRENT_USER_RESPONDED,
        status: FetchCurrentUserResponseStatus.LOGGED_IN,
        user: resp.data,
      });
    }
    catch (e) {
      console.log(e);
      yield put({
        type: FETCH_CURRENT_USER_RESPONDED,
        status: FetchCurrentUserResponseStatus.INVALID_TOKEN,
      });
    }
  } else {
    yield put({
      type: FETCH_CURRENT_USER_RESPONDED,
      status: FetchCurrentUserResponseStatus.NO_TOKEN,
    });
  }
}

function* fetchUserProfile({id}) {
  try {
    const url = `${addrBase}users/${id}/`;
    const resp = yield call(axios.get, url);
    yield put({type: FETCH_USER_PROFILE_RESPONDED,
    user: resp.data})
  } catch (e) {
    console.log(e);
  }
}

function* fetchUserNews({id, page}) {
  try {
    const url = `${addrBase}news/author_specific/?page=${page}&user=${id}`;
    const resp =  yield call(axios.get, url);
    yield put({type: FETCH_USER_NEWS_RESPONDED,
      news: resp.data});
  } catch (e) {
    console.log(e);
  }
}

function* postNews({news}) {
  const token = localStorage.getItem('access_token');

  const data = new FormData();
  data.append('theme', news.theme);
  data.append('content', news.content);
  data.append('tags', news.tags);
  if(news.image)
    data.append('image', news.image);

  const options = {
    method: 'POST',
    url: `${addrBase}news/`,
    headers: {
      Authorization: `Bearer ${token}`,
      'Content-Type': 'multipart/form-data'
    },
    data: data,
  };

  try {
    yield call(axios, options);
    const userId = yield select(takeUserIdFromState);
    yield put({type: FETCH_USER_NEWS, id: userId});
    yield put({type: SET_PROFILE_MODALS_DISPLAY_STATE,
      state: ProfileModalsDisplayMode.HIDE})
  } catch (e) {
    console.log(e);
  }
}

function* editProfile({user, userId}) {
  const token = yield localStorage.getItem('access_token');

  const data = new FormData();
  for(let field in user){
    if(user[field])
      data.append(field, user[field]);
  }

  const options = {
    method: 'PATCH',
    url: `${addrBase}users/${userId}/`,
    headers: {
      Authorization: `Bearer ${token}`,
      'Content-Type': 'multipart/form-data'
    },
    data: data,
  };

  try {
    yield call(axios, options);

    yield put({type: FETCH_USER_PROFILE, id: userId});
    yield put({type: SET_PROFILE_MODALS_DISPLAY_STATE,
      state: ProfileModalsDisplayMode.HIDE})
  } catch (e) {
    console.log(e);
  }
}

export default function* watcherSaga() {
  yield takeEvery(NEWS_REQUESTED, fetchNews);
  yield takeEvery(TRY_LOGIN_STANDARD, tryLoginStd);
  yield takeEvery(TRY_AUTH_GOOGLE, tryLoginGoogle);
  yield takeEvery(TRY_REGISTER_STANDARD, tryRegisterStd);
  yield takeEvery(FETCH_CURRENT_USER, getCurrentUserData);
  yield takeEvery(LOGOUT, logout);
  yield takeEvery(FETCH_USER_PROFILE, fetchUserProfile);
  yield takeEvery(FETCH_USER_NEWS, fetchUserNews);
  yield takeEvery(POST_NEWS, postNews);
  yield takeEvery(EDIT_PROFILE, editProfile);
}
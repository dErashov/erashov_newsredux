import { createStore, applyMiddleware, compose } from 'redux'
import createSagaMiddleware from 'redux-saga'
import { createBrowserHistory } from 'history'
import createRootReducer from '../reducers'

import rootSaga from "../sagas/api-saga";

export const history = createBrowserHistory();

const sagaMiddleware = createSagaMiddleware();

const enhancers = [];
const middleware = [
  sagaMiddleware,
];

const composedEnhancers = compose(
  applyMiddleware(...middleware),
  ...enhancers
);

const store = createStore(
    createRootReducer(history),
    composedEnhancers
  );

sagaMiddleware.run(rootSaga);

export default store
import store from './store/index'
import {addNews} from "./actions";

window.store = store;
window.addNews = addNews;